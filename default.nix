{ pkgs ? import <nixpkgs> {} }:

rec {
  xsecurelock = pkgs.callPackage ./pkgs/xsecurelock {};
}
